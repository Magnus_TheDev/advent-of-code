const { getData, intersection } = require('../utils')

const data = getData('day3').trim().split('\n')

function findSharedItem(...strs) {
  const sets = strs.map(str => new Set([...str]))
  const sharedItem = intersection(...sets)
  return Array.from(sharedItem)[0]
}

const charOrder = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'.split('')

console.log(data.map(item => [item.slice(0, item.length / 2), item.slice(item.length / 2)])
.map(compartments => findSharedItem(...compartments))
.map(item => (charOrder.findIndex(char => char === item)+1))
.reduce((x, y) => x + y, 0))

console.log(data.reduce((r, e, i) => (i % 3 ? r[r.length - 1].push(e) : r.push([e])) && r, []).map(group => findSharedItem(...group))
.map(group => (charOrder.findIndex(char => char === group)+1)).reduce((x, y) => x + y, 0))
