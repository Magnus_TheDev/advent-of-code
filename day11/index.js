const { getData, splitData } = require('../utils')

const input = getData('day9').split('\n\n')
  .map((monkey) => {
    const items = monkey
      .match(/Starting items: ((\d+(,\s)?)+)/)[1]
      .split(', ')
      .map(Number)


      /* REGEX */
    let operation = monkey.match(/Operation: new = (.+)/)[1].replace(/old/g, '${old}')
    operation = `\`${operation}\``
    const test = Number(monkey.match(/Test: divisible by (\d+)/)[1])
    const True = Number(monkey.match(/If true: throw to monkey (\d+)/)[1])
    const False = Number(monkey.match(/If false: throw to monkey (\d+)/)[1])

    return { items, operation, test, True, False, inspections: 0 }
  })


function part1(){
    let part1 = input;
    for (let round = 1; round <= 20; round++) {
        part1.forEach(({ items, operation, test, True, False }, monkey) => {
            items.forEach((old) => {
                let item = eval(eval(operation))
                item = Math.floor(item / 3)
                part1[monkey].inspections++
                const newMonkey = item % test === 0 ? True : False
                part1[newMonkey].items.push(item)
            })
            part1[monkey].items = []
        })
    }

    const [a, b, _] = part1
    .map((x) => x.inspections)
    .sort((a, b) => a - b)
    .reverse()
    return a * b
}
 console.log(part1())