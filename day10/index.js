const { getData, splitData } = require('../utils')

const lines = getData('day9').split('\n')
let cycle = 0;
let sum = 0;
let x = 1;
let result = '';

function progress(times = 1, version = 1) {
    if(version === 1){
        for(let i = 0; i< times; i++){
            cycle++;
            if ([20, 60, 100, 140, 180, 220].includes(cycle)) {
                sum += x * cycle;
            }
        }
    }else{
        for(let i = 0; i< times; i++){
            if (cycle % 40 === 0){
                result += '\n';
            }
            result += Math.abs((cycle % 40) - x) <= 1 ? '#' : '.';
            cycle++;
        }
    }
}

function part1() {
    for (const line of lines) {
        if (line === 'noop'){
            progress();
        }
        else {
            progress(2);
            x += +line.split(' ').pop();
        }
    }
    return sum;
}

console.log(part1());

function part2() {
    cycle = 0;
    sum = 0;
    x = 1;
    result = '';

    for (const line of lines) {
        if (line === 'noop'){
            progress(1,2)
        }
        else {
            progress(2,2)
            x += +line.split(' ').pop();
        }
    }
    return result;
}

console.log(part2());