const { getData } = require('../utils')

const input = getData('day7').split('\n')

const SPACE = 40000000;

const MAX_SIZE = 100000;


const dirSize = function() {
  let dirs = [];
  const sizes = {};
  for (const line of input) {
    if (line === "$ cd /") dirs = [];
    if (line === "$ cd ..") dirs.pop();
      if (line.match(/\$ cd ((?!\.\.).+)/g)){
          dirs.push(line.match(/\$ cd ((?!\.\.).+)/g)[0].slice(5));
    }
      if (line.match(/(\d+) .+/g)){
        dirFormat = dirs.reduce((accumulator, currentValue) => [...accumulator,(accumulator[accumulator.length - 1] ? accumulator[accumulator.length - 1] + "." : "") + currentValue]);
        for (const dir of dirFormat){
            if(sizes[dir] != undefined)
                {
                    sizes[dir] = sizes[dir] + Number(line.match(/(\d+) .+/g)[0].split(' ')[0]);
                }else{
                sizes[dir] = Number(line.match(/(\d+) .+/g)[0].split(' ')[0]);
            }
        }
    }

  }

    return [Object.values(sizes), Object.values(sizes)[0]];
};

function part1(){
    let total = 0;
    dirSize()[0].forEach(currentItem => {
        if(currentItem <= MAX_SIZE){
            total += currentItem
        }
    });
    return total;
};

console.log(part1());
console.log(dirSize()[0].filter((size) => size >= dirSize()[1] - SPACE).sort((a, b) => a - b )[0]);