const { getData, intersection, createArrayFrom } = require('../utils')


console.log(getData('day4').trim()
.split('\n')
.map(str =>
str.split(',').map(str => {
    const [from, to] = str.split('-').map(Number)
    return { from, to }
}))
.map(([a, b]) => a.from <= b.from && a.to >= b.to || a.from <= b.from && a.to >= b.to)
.filter(Boolean).length)

console.log(getData('day4').trim()
    .split('\n')
    .map(str =>
str.split(',').map(str => {
    const [from, to] = str.split('-').map(Number)
    return { from, to }
}))
.map(([a, b]) => intersection(new Set((createArrayFrom(a.from, a.to))), new Set(createArrayFrom(b.from, b.to))).size > 0)
.filter(Boolean).length)
