const { getData, intersection, createArrayFrom } = require('../utils')


function part1(input, len = 4) {
    for (let i = 0; i < input.length; i++) {
        const slice = input.slice(i, i + len);
        if (new Set(slice.split('')).size === len) {
            return i + len;
        }
    }
}

function part2(input) {
    return part1(input, 14);
}


part1(getData('day6').trim())
part2(getData('day6').trim())