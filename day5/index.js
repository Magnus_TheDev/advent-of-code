const { getData } = require('../utils')

const parseInput = ([show, moves]) => {
    show = show.split('\n').map(line => line.match(/.{1,4}/g).map(crate => crate.replace(/[\[\] ]/g, '')))
    stack = new Array(show.pop().length).fill([])

    show.reverse().forEach((line) => {
        line.forEach((crate, index) => {
          if(crate !== '')
          {
              stack[index] = [...stack[index], crate]
          }
        })
    })


    moves = moves.split('\n').map(line => {
        let crates = line.split(' ').map(n => parseInt(n))[1]
        let start = line.split(' ').map(n => parseInt(n))[3] -1
        let end = line.split(' ').map(n => parseInt(n))[5] -1
        return {crates, start: start, end: end}
      })
  return {moves, stack}
}

const part1 = ({moves, stack}) => {
  moves.forEach(({crates, start, end})=> {stack[end] = [...stack[end], ...stack[start].splice(stack[start].length-crates, crates).reverse()]})
  return stack.map(col => col.pop()).join('')
}

const part2 = ({moves, stack}) => {
  moves.forEach(({crates, start, end})=> {stack[end] = [...stack[end], ...stack[start].splice(stack[start].length-crates, crates)]})
  return stack.map(col => col.pop()).join('')
}

console.log(part1(parseInput(getData('day5').split('\n\n'))))
console.log(part2(parseInput(getData('day5').split('\n\n'))))