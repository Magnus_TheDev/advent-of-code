const { getData, trace, splitData } = require('../utils');

const data = getData('day2')

function getPairs(input) {
    return splitData(input)
}

const pointsType = {
  X: 1,
  Y: 2,
  Z: 3,
}

const resultPoints = {
  l: 0,
  d: 3,
  w: 6,
}

const RESULT = {
  A: {
    X: 'd',
    Y: 'w',
    Z: 'l',
  },
  B: {
    X: 'l',
    Y: 'd',
    Z: 'w',
  },
  C: {
    X: 'w',
    Y: 'l',
    Z: 'd',
  },
}

const ResultByType = {
    X: 'l',
    Y: 'd',
    Z: 'w',
}

const resultNeeded = {
    A: {
        X: 'Z',
        Y: 'X',
        Z: 'Y',
    },
    B: {
        X: 'X',
        Y: 'Y',
        Z: 'Z',
    },
    C: {
        X: 'Y',
        Y: 'Z',
        Z: 'X',
    },
}

function part1(input) {
  return (
    getPairs(input)
    .map(([crypt, crypt2]) => {
        return [crypt2, RESULT[crypt][crypt2]]
    })
      .map(
        ([type, result]) => {
            return pointsType[type] + resultPoints[result]
        }
      )
    .reduce((x, y) => x + y, 0)
  )
}

function part2(input) {
  return (
    getPairs(input)
    .map(([them, desiredResult]) => {
        return [
            resultNeeded[them][desiredResult],
            ResultByType[desiredResult],
        ]
    }
        )
      .map(
        ([type, result]) =>{
            return pointsType[type] + resultPoints[result]
        }
      )
    .reduce((x, y) => x + y, 0)
  )
}

console.log(part1(data));
console.log(part2(data))
