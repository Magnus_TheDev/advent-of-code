const fs = require('fs')
const path = require('path')

function getData(dir) {
    return fs.readFileSync(path.resolve('./input.txt'), {
        encoding: 'utf-8',
    })
}

const splitData = data => data.trim().split('\n').map(str => str.split(' '))

const trace = msg => x => (console.log(msg, x), x)

function createArrayFrom(from, to) {
    const result = []

    for (let i = from; i <= to; i++) {
        result.push(i)
    }

    return result
}

function intersection(...sets) {
    return sets.reduce((set1, set2) => {
        const result = new Set()

        for (const item of set2) {
            if (set1.has(item)) {
                result.add(item)
            }
        }
        return result
    })
}

module.exports = {
    createArrayFrom,
    intersection,
    getData,
    trace,
    splitData
}