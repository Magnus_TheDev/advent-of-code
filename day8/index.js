const { getData } = require('../utils')

const data = getData('day8')

function isTreeVisible(height, rowId, colId, grid) {
    const visibleFromLeft = grid[rowId].slice(0, colId).every(tree => tree < height)
    const visibleFromRight = grid[rowId].slice(colId + 1).every(tree => tree < height)
    const visibleFromTop = grid.slice(0, rowId).map(row => row[colId]).every(tree => tree < height)
    const visibleFromBottom = grid.slice(rowId + 1).map(row => row[colId]).every(tree => tree < height)
    return (visibleFromLeft || visibleFromRight || visibleFromTop || visibleFromBottom)
}

function part1(input) {
    const Row = data.trim().split('\n').map(row => row.split('').map(Number))
    let count = 0

    for (i = 0; i < Row.length; i++) {
        for (w = 0; w < Row[0].length; w++) {
            const tree = Row[i][w]

            if (isTreeVisible(tree, i, w, Row)) {
                count++
            }
        }
    }

    return count
}

console.log(part1(data));